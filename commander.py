
class Commander:
    def _setup_commander(self, name):
        self._commander = name

        self._commands = {}
        self._update_cmd()
        self._aliases = {}
        self._reverse_alias_index = {}

    def _add_alias(self, oldn, newn):
        if self._aliases.get(oldn) == None:
            self._aliases[oldn] = []

        self._aliases[oldn].append(newn)
        self._reverse_alias_index[newn] = oldn

    def _run_cmd(self, name):
        if self._commands.get(name):
            return self._commands[name]()
        else:
            if self._reverse_alias_index.get(name):
                for alias in self._aliases[self._reverse_alias_index.get(name)]:
                    if self._commands.get(self._reverse_alias_index[alias]):
                        return self._commands[self._reverse_alias_index[alias]]()

            return self._commands["default"]()

    def _update_cmd(self):
        for fn in dir(self):
            if fn.startswith(f"_{self._commander}_"):
                self._commands[fn.replace(f"_{self._commander}_", "")] = getattr(self, fn)


class OptCommand:
    def __init__(self, ls):
        self.commands = []
        self.flags = {}
        self.filename = ls[0]
        for l in ls[1:]:
            if l.startswith("--") and (":" in l):
                cmd = l.split(":")[0][2:]
                line = ":".join(l.split(":")[1:])
                if line in ["yes", "true"]:
                    self.flags[cmd] = True
                elif line in ["no", "false"]:
                    self.flags[cmd] = False
                else:
                    try:
                        self.flags[cmd] = int(line)
                    except:
                        self.flags[cmd] = line
            elif l.startswith("--") and ":" not in l:
                self.flags[l[2:]] = True
            elif l.startswith("-") and ":" not in l:
                self.flags[l[1:]] = True
            elif not l.startswith("-") and ":" not in l:
                self.commands.append(l)
            else:
                print(f"Parser error: {l}")

    def command(self, ind = 0):
        #print(len(self.commands), ind)
        if len(self.commands) <= ind:
            return ""
        else:
            return self.commands[ind]

    def flag(self, name, deflt = None):
        return self.flags.get(name, deflt)