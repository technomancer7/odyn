import json, sys, os
import commander, arrow
from colorama import init as colorama_init
from colorama import Fore, Back, Style
from odyn_ui import runGUI

colorama_init(autoreset=True)

Arg = commander.OptCommand(sys.argv)

# TODO
# move the cmd specific stuff in to a master class for future modularity


class Odyn(commander.Commander):
    def __init__(self):
        self.data = {}
        self.load_data()
        self._setup_commander("odyn")
        self._add_alias("list", "ls")
    def load_data(self):
        try:
            with open(os.path.expanduser("~")+"/odin.json") as f:
                d = json.load(f)
                self.data = d
        except: 
            self.data = {}

        return self.data

    def save_data(self):
        with open(os.path.expanduser("~")+"/odin.json", "w+") as f:
            return json.dump(self.data, f, indent=4)

    def get_time(self, ts):
        return arrow.get(ts, "YYYY-MMM-DD HH:mm:ss")

    def export_time(self):
        tz = self.get_var("timezone")
        m = arrow.now(tz)
        return m.format("YYYY-MMM-DD HH:mm:ss")

    def humanize_time(self, d):
        return self.get_time(d["dt"]).shift(hours=-1).humanize(granularity=["month", "day", "hour"])

    def add_new_note(self, line):
        new_note = {"dt": self.export_time(), "group": "", "text": line}
        self.data["notes"].append(new_note)
        self.save_data()

    def delete_note(self, index):
        del self.data["notes"][index]
        self.save_data()

    def get_note(self, index):
        if index >= len(self.data["notes"]):
            print("Must be below", len(self.data["notes"]))
            return None

        return self.data["notes"][index]

    def validate_note_index(self, index):
        if index >= len(self.data["notes"]):
            return False

        return True
    
    def tag_note(self, index, tag):
        #d = self.get_note(index)
        if self.validate_note_index(index):
            if not self.data["notes"][index].get("tags"):
                self.data["notes"][index]["tags"] = []

            self.data["notes"][index]["tags"].append(tag)
            return True
        return False

    def untag_note(self, index, tag):
        #d = self.get_note(index)
        if self.validate_note_index(index):
            if not self.data["notes"][index].get("tags"):
                self.data["notes"][index]["tags"] = []

            self.data["notes"][index]["tags"].append(tag)
            return True
        return False   

    def group_note(self, index, group = ""):
        #d = self.get_note(index)
        if self.validate_note_index(index):
            self.data["notes"][index]["group"] = group
            return True
        return False

    def rewrite_note(self, index, new_text = ""):
        #d = self.get_note(index)
        if self.validate_note_index(index):
            self.data["notes"][index]["text"] = new_text
            return True
        return False

    def replace_in_note(self, index, old_text, new_text = ""):
        #TODO make it a regex replace
        #d = self.get_note(index)
        if self.validate_note_index(index):
            self.data["notes"][index]["text"] = self.data["notes"][index]["text"].replace(old_text, new_text)
            return True
        return False

    def edit_note(self, index, key, value):
        #d = self.get_note(index)
        if self.validate_note_index(index):
            self.data["notes"][index][key] = value
            return True
        return False

    def set_var(self, key, val):
        self.data["settings"][key] = val

    def get_var(self, key, deflt = None):
        return self.data["settings"].get(key, deflt)

    def format_note(self, index, data):
        ind = f"[{Fore.CYAN}#{index}{Style.RESET_ALL}"
        if data["group"]:
            if ind == "":
                ind = f"[{Fore.RED}{data['group']}{Style.RESET_ALL}]"
            else:
                ind += f":{Fore.RED}{data['group']}{Style.RESET_ALL}"
        ind += "]"
        created_at = self.humanize_time(data)
        f = f"""| {ind} Created {Fore.YELLOW}{created_at}{Style.RESET_ALL}."""

        for ln in data["text"].split("\n"):
            f += f"\n| {Fore.BLUE}"+ln+Style.RESET_ALL

        if data.get("tags"):
            f += f"\n| Tags: {Fore.GREEN}{','.join(data['tags'])}"
        return f

    def print_note(self, index):
        if type(index) == int:
            if self.validate_note_index(index):
                print(self.format_note(index, self.get_note(index)))

        if type(index) == dict:
            print(self.format_note("?", index))

    def is_note_tagged(self, index, tag):
        if type(index) == int:
            if self.validate_note_index(index):
                f = self.get_note(index)
                if f.get("tags") and tag in f["tags"]:
                    return True

        if type(index) == dict:
            if index.get("tags") and tag in index["tags"]:
                return True
        return False

    def is_note_group(self, index, grp):
        if type(index) == int:
            if self.validate_note_index(index):
                f = self.get_note(index)
                if grp == f["group"]:
                    return True

        if type(index) == dict:
            if grp == index["group"]:
                return True
        return False

    # Commands array
    def _odyn_add(self):
        """
        Adds a new note.
        Adds optional paramaters;
        --tag:tag1[,tag1] - defines tags for the new note
        --group:group - adds note to group name
        """
        c = " ".join(Arg.commands[1:]).strip()
        if len(c) == 0:
            print("No text given.")
            return

        self.add_new_note(c)

    def _odyn_delete(self):
        """
        delete <index>
        Deletes note.
        """
        if Arg.command(1):
            try:
                f = int(Arg.command(1))
                if self.validate_note_index(f):
                    self.print_note(f)
                    if input("Delete this note? (y)> ") == "y":
                        self.delete_note(f)
                        print("Deleted.")
                else:
                    print("Not a valid index.")
            except:
                print("Value is not a number.")

    def _odyn_rewrite(self):
        """
        rewrite <index> <new_text>
        Replaces the text of a note.
        """
        pass

    def _odyn_replace(self):
        """
        replace <index> <regex> <new>
        Replaces pattern in text with new text.
        """
        pass

    def _odyn_tag(self):
        """
        tag <index> <new_tag>
        Adds tag to note.
        """
        pass

    def _odyn_untag(self):
        """
        untag <index> <tag>
        Removes tag to note.
        """
        pass

    def _odyn_group(self):
        """
        group <index> <name>
        Assigns note to group name.
        """
        pass 
    
    def _odyn_gui(self):
        """
        Opens the GUI.
        """
        pass

    def _odyn_list(self):
        """
        Shows a list of all notes.
        Can be filtered by 
        --dt:date - support different dt formats to find notes from same day
        --includes:text_in_note - checks text in note
        --tagged:tag1[,tag2] - checks if tags exist in the note
        --group:group_name - checks if note is in group
        """
        print("------------")
        for entry in range(0, len(self.data["notes"])):
            self.print_note(entry)
            print("------------")

    def _odyn_show(self):
        """
        show <index>
        Shows the note.
        """
        if self.validate_note_index(int(Arg.command(1))):
            self.print_note(int(Arg.command(1)))
        else:
            print("Must be below ",len(self.data["notes"]))

    def _odyn_help(self):
        if len(Arg.commands) == 1:
            for c in self._commands:
                if c != "default": 
                    cmds = []
                    cmds.append(c)
                    if self._aliases.get(c):
                        for com in self._aliases[c]:
                            cmds.append(com)
                    print(', '.join(cmds))
            return

        if Arg.command(1) and self._commands.get(Arg.command(1)) and self._commands[Arg.command(1)].__doc__:
            print(self._commands[Arg.command(1)].__doc__)
            if self._aliases.get(Arg.command(1)):
                print(f"\tAliases: {Fore.BLUE}{', '.join(self._aliases[Arg.command(1)])}")
        else:
            print("Problem finding help for that name.")

    def _odyn_default(self):
        print("Invalid command.")

def main():
    odyn = Odyn()
    odyn._run_cmd(Arg.command())

if __name__ == "__main__":
    main()